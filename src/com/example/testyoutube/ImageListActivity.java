/*******************************************************************************
 * Copyright 2011-2013 Sergey Tarasevich
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package com.example.testyoutube;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

public class ImageListActivity extends AbsListViewBaseActivity {

	DisplayImageOptions options;

	public static ArrayList<YoutubeObject> array;

	String[] titles;
	String[] imageUrls;
	String[] vCodes;
	
	String nameVideo;
	
	boolean isPlay = false;
	
	final String strPref_Download_ID = "PREF_DOWNLOAD_ID_MAZ";
//	String Download_path = "";
	String Download_ID = "DOWNLOAD_ID_MAZ";
	SharedPreferences preferenceManager;
	DownloadManager downloadManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_list);

		BaseActivity.imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		
		// Bundle bundle = getIntent().getExtras();
		// imageUrls = bundle.getStringArray(Extra.IMAGES);

		if (array != null) {

			titles = new String[array.size()];
			imageUrls = new String[array.size()];
			vCodes = new String[array.size()];

			for (int i = 0; i < array.size(); i++) {
				YoutubeObject youtubeObject = (YoutubeObject) array.get(i);
				titles[i] = youtubeObject.getTitle();
				imageUrls[i] = youtubeObject.getCover();
				vCodes[i] = youtubeObject.getVcode();
			}
		}

		options = new DisplayImageOptions.Builder()
				.showStubImage(R.drawable.ic_stub)
				.showImageForEmptyUri(R.drawable.ic_empty)
				.showImageOnFail(R.drawable.ic_error).cacheInMemory(true)
				.cacheOnDisc(true).displayer(new FadeInBitmapDisplayer(500)/*RoundedBitmapDisplayer(20)*/)
				.build();

		listView = (ListView) findViewById(android.R.id.list);
		((ListView) listView).setAdapter(new ItemAdapter());
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				System.out.println("vcode = " + vCodes[position]);
				
//				downloadYoutube(vCodes[position], titles[position]);
				
				Intent intent = YouTubeStandalonePlayer.createVideoIntent(ImageListActivity.this, "AIzaSyBuJweYhLFmX3fvlKhWkv6lnZqNlItSlcs", vCodes[position]);
				 startActivity(intent);
				
//				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://" + vCodes[position]));
//				startActivity(intent);
				
//				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v="+vCodes[position])));
				
				isPlay = true;
			}
		});
		
		preferenceManager = PreferenceManager.getDefaultSharedPreferences(this);
		downloadManager	= new DownloadManager(getContentResolver(), "com.example.testyoutube");

	}
	
	private void downloadYoutube(String vcode, String name){
		nameVideo = name;
		new YouTubePageStreamUriGetter().execute("https://www.youtube.com/watch?v="+vcode);
	}
	


	class Meta {
	    public String num;
	    public String type;
	    public String ext;

	    Meta(String num, String ext, String type) {
	        this.num = num;
	        this.ext = ext;
	        this.type = type;
	    }
	}

	class Video {
	    public String ext = "";
	    public String type = "";
	    public String url = "";

	    Video(String ext, String type, String url) {
	        this.ext = ext;
	        this.type = type;
	        this.url = url;
	    }
	}

	public ArrayList<Video> getStreamingUrisFromYouTubePage(String ytUrl)
	        throws IOException {
	    if (ytUrl == null) {
	        return null;
	    }

	    // Remove any query params in query string after the watch?v=<vid> in
	    // e.g.
	    // http://www.youtube.com/watch?v=0RUPACpf8Vs&feature=youtube_gdata_player
	    int andIdx = ytUrl.indexOf('&');
	    if (andIdx >= 0) {
	        ytUrl = ytUrl.substring(0, andIdx);
	    }

	    // Get the HTML response
	    String userAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:8.0.1)";
	    HttpClient client = new DefaultHttpClient();
	    client.getParams().setParameter(CoreProtocolPNames.USER_AGENT,
	            userAgent);
	    HttpGet request = new HttpGet(ytUrl);
	    HttpResponse response = client.execute(request);
	    String html = "";
	    InputStream in = response.getEntity().getContent();
	    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
	    StringBuilder str = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	        str.append(line.replace("\\u0026", "&"));
	    }
	    in.close();
	    html = str.toString();

	    // Parse the HTML response and extract the streaming URIs
	    if (html.contains("verify-age-thumb")) {
	        System.out.println("YouTube is asking for age verification. We can't handle that sorry.");
	        return null;
	    }

	    if (html.contains("das_captcha")) {
	    	System.out.println("Captcha found, please try with different IP address.");
	        return null;
	    }

	    Pattern p = Pattern.compile("stream_map\": \"(.*?)?\"");
	    // Pattern p = Pattern.compile("/stream_map=(.[^&]*?)\"/");
	    Matcher m = p.matcher(html);
	    List<String> matches = new ArrayList<String>();
	    while (m.find()) {
	        matches.add(m.group());
	    }

	    if (matches.size() != 1) {
	    	System.out.println("Found zero or too many stream maps.");
	        return null;
	    }

	    String urls[] = matches.get(0).split(",");
	    HashMap<String, String> foundArray = new HashMap<String, String>();
	    for (String ppUrl : urls) {
	        String url = URLDecoder.decode(ppUrl, "UTF-8");

	        Pattern p1 = Pattern.compile("itag=([0-9]+?)[&]");
	        Matcher m1 = p1.matcher(url);
	        String itag = null;
	        if (m1.find()) {
	            itag = m1.group(1);
	        }

	        Pattern p2 = Pattern.compile("sig=(.*?)[&]");
	        Matcher m2 = p2.matcher(url);
	        String sig = null;
	        if (m2.find()) {
	            sig = m2.group(1);
	        }

	        Pattern p3 = Pattern.compile("url=(.*?)[&]");
	        Matcher m3 = p3.matcher(ppUrl);
	        String um = null;
	        if (m3.find()) {
	            um = m3.group(1);
	        }

	        if (itag != null && sig != null && um != null) {
	            foundArray.put(itag, URLDecoder.decode(um, "UTF-8") + "&"
	                    + "signature=" + sig);
	        }
	    }

	    if (foundArray.size() == 0) {
	    	System.out.println("Couldn't find any URLs and corresponding signatures");
	        return null;
	    }

	    HashMap<String, Meta> typeMap = new HashMap<String, Meta>();
	    typeMap.put("13", new Meta("13", "3GP", "Low Quality - 176x144"));
	    typeMap.put("17", new Meta("17", "3GP", "Medium Quality - 176x144"));
	    typeMap.put("36", new Meta("36", "3GP", "High Quality - 320x240"));
	    typeMap.put("5", new Meta("5", "FLV", "Low Quality - 400x226"));
	    typeMap.put("6", new Meta("6", "FLV", "Medium Quality - 640x360"));
	    typeMap.put("34", new Meta("34", "FLV", "Medium Quality - 640x360"));
	    typeMap.put("35", new Meta("35", "FLV", "High Quality - 854x480"));
	    typeMap.put("43", new Meta("43", "WEBM", "Low Quality - 640x360"));
	    typeMap.put("44", new Meta("44", "WEBM", "Medium Quality - 854x480"));
	    typeMap.put("45", new Meta("45", "WEBM", "High Quality - 1280x720"));
	    typeMap.put("18", new Meta("18", "MP4", "Medium Quality - 480x360"));
	    typeMap.put("22", new Meta("22", "MP4", "High Quality - 1280x720"));
	    typeMap.put("37", new Meta("37", "MP4", "High Quality - 1920x1080"));
	    typeMap.put("33", new Meta("38", "MP4", "High Quality - 4096x230"));

	    ArrayList<Video> videos = new ArrayList<ImageListActivity.Video>();

	    for (String format : typeMap.keySet()) {
	        Meta meta = typeMap.get(format);

	        if (foundArray.containsKey(format)) {
	            Video newVideo = new Video(meta.ext, meta.type,
	                    foundArray.get(format));
	            videos.add(newVideo);
	            System.out.println("YouTube Video streaming details: ext:" + newVideo.ext
	                    + ", type:" + newVideo.type);
	            
	            System.out.println("URL YOUTUBE: " + newVideo.url);
	            startDownload(newVideo.url);
	            
	        }
	    }

	    return videos;
	}
	
	String nowPlayVideo = "";
	private void startDownload(String url){
		if(nowPlayVideo != url){
			nowPlayVideo = url;
			//play
			Intent intent = new Intent(Intent.ACTION_VIEW); 
            intent.setDataAndType(Uri.parse(nowPlayVideo), "video/mp4"); 
            startActivity(intent); 
            
            
            
			//download
//			Uri Download_Uri = Uri.parse(nowPlayVideo);
//			DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
//
////			request.setDestinationInExternalPublicDir(
////					Environment.DIRECTORY_DOWNLOADS + "/Doujin/",doujinname);
//			request.setDestinationUri(Uri.fromFile(new File( Environment.getExternalStorageDirectory(),nameVideo+".mp4") ));
//
//			long download_id = downloadManager.enqueue(request);
//
//			// Save the download id
//			Editor PrefEdit = preferenceManager.edit();
//			PrefEdit.putLong(Download_ID, download_id);
//			PrefEdit.commit();
//			
//			
//			Toast.makeText(this,
//					"Start download.",
//					Toast.LENGTH_SHORT).show();
		}
		
		
		
//		Uri uri =  Uri.parse(url);
//		
//		DownloadManager.Request req=new DownloadManager.Request(uri);
//
//		req.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI
//		                               | DownloadManager.Request.NETWORK_MOBILE)
//		   .setAllowedOverRoaming(false)
//		   .setTitle(nameVideo)
//		   .setDescription("Something useful. No, really.")
//		   .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
//				   nameVideo+".mp4");
	}

	private class YouTubePageStreamUriGetter extends
	        AsyncTask<String, String, String> {
	    ProgressDialog progressDialog;

	    @Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        progressDialog = ProgressDialog.show(ImageListActivity.this, "",
	                "Connecting to YouTube...", true);
	    }

	    @Override
	    protected String doInBackground(String... params) {
	        String url = params[0];
	        try {
	            ArrayList<Video> videos = getStreamingUrisFromYouTubePage(url);
	            if (videos != null && !videos.isEmpty()) {
	                String retVidUrl = null;
	                for (Video video : videos) {
	                    if (video.ext.toLowerCase().contains("mp4")
	                            && video.type.toLowerCase().contains("medium")) {
	                        retVidUrl = video.url;
	                        break;
	                    }
	                }
	                if (retVidUrl == null) {
	                    for (Video video : videos) {
	                        if (video.ext.toLowerCase().contains("3gp")
	                                && video.type.toLowerCase().contains(
	                                        "medium")) {
	                            retVidUrl = video.url;
	                            break;

	                        }
	                    }
	                }
	                if (retVidUrl == null) {

	                    for (Video video : videos) {
	                        if (video.ext.toLowerCase().contains("mp4")
	                                && video.type.toLowerCase().contains("low")) {
	                            retVidUrl = video.url;
	                            break;

	                        }
	                    }
	                }
	                if (retVidUrl == null) {
	                    for (Video video : videos) {
	                        if (video.ext.toLowerCase().contains("3gp")
	                                && video.type.toLowerCase().contains("low")) {
	                            retVidUrl = video.url;
	                            break;
	                        }
	                    }
	                }

	                return retVidUrl;
	            }
	        } catch (Exception e) {
	        	System.out.println("Couldn't get YouTube streaming URL: "+ e.getMessage());
	        }
	        System.out.println("Couldn't get stream URI for " + url);
	        return null;
	    }

	    @Override
	    protected void onPostExecute(String streamingUrl) {
	        super.onPostExecute(streamingUrl);
	        progressDialog.dismiss();
	        if (streamingUrl != null) {
	                         /* Do what ever you want with streamUrl */
	        }
	    }
	}
	
	
	@Override
	public void onBackPressed() {
		AnimateFirstDisplayListener.displayedImages.clear();
		super.onBackPressed();
	}

	class ItemAdapter extends BaseAdapter {

		private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

		private class ViewHolder {
			public TextView text;
			public ImageView image;
		}

		@Override
		public int getCount() {
			return imageUrls.length;
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View view = convertView;
			final ViewHolder holder;
			if (convertView == null) {
				view = getLayoutInflater().inflate(R.layout.youtube_item,
						parent, false);
				holder = new ViewHolder();
				holder.text = (TextView) view.findViewById(R.id.text);
				holder.image = (ImageView) view.findViewById(R.id.image);
				view.setTag(holder);
			} else {
				holder = (ViewHolder) view.getTag();
			}

			holder.text.setText(titles[position]);
			imageLoader.displayImage(imageUrls[position], holder.image,
					options, animateFirstListener);

			return view;
		}
	}
	
	
	

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}
	
	
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		if(isPlay){
			isPlay = false;
			Intent i = new Intent(getApplicationContext(), AdsWrapper.class);
	        i.putExtra("partnerId", "101021");
	        i.putExtra("appId", "com.example.testyoutube");
	        i.putExtra("showAt", "start");
	        i.putExtra("skipEarly", "true");
	        i.putExtra("adsTimeout", "10");
	        startActivity(i);
		}
		

		//download
//		IntentFilter intentFilter 
//			= new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
//		registerReceiver(downloadReceiver, intentFilter);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		unregisterReceiver(downloadReceiver);
	}

	private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context arg0, Intent arg1) {
			CheckDwnloadStatus();
			// TODO Auto-generated method stub
			DownloadManager.Query query = new DownloadManager.Query();
			query.setFilterById(preferenceManager.getLong(strPref_Download_ID, 0));
			Cursor cursor = downloadManager.query(query);
			if(cursor.moveToFirst()){
				int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
				int status = cursor.getInt(columnIndex);
				if(status == DownloadManager.STATUS_SUCCESSFUL){

					//Retrieve the saved request id
					long downloadID = preferenceManager.getLong(strPref_Download_ID, 0);

					ParcelFileDescriptor file;
					try {
						file = downloadManager.openDownloadedFile(downloadID);
						Toast.makeText(ImageListActivity.this,
								"File Downloaded: " + file.toString(),
								Toast.LENGTH_LONG).show();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						Toast.makeText(ImageListActivity.this,
								e.toString(), Toast.LENGTH_LONG).show();
					}
				}
			}
		}	
	};
	private void CheckDwnloadStatus(){

		// TODO Auto-generated method stub
		DownloadManager.Query query = new DownloadManager.Query();
		long id = preferenceManager.getLong(strPref_Download_ID, 0);
		query.setFilterById(id);
		Cursor cursor = downloadManager.query(query);
		if(cursor.moveToFirst()){
			int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
			int status = cursor.getInt(columnIndex);
			int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
			int reason = cursor.getInt(columnReason);

			switch(status){
				case DownloadManager.STATUS_FAILED:
				String failedReason = "";
				switch(reason){
					case DownloadManager.ERROR_CANNOT_RESUME:
					failedReason = "ERROR_CANNOT_RESUME";
					break;
					case DownloadManager.ERROR_DEVICE_NOT_FOUND:
					failedReason = "ERROR_DEVICE_NOT_FOUND";
					break;
					case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
					failedReason = "ERROR_FILE_ALREADY_EXISTS";
					break;
					case DownloadManager.ERROR_FILE_ERROR:
					failedReason = "ERROR_FILE_ERROR";
					break;
					case DownloadManager.ERROR_HTTP_DATA_ERROR:
					failedReason = "ERROR_HTTP_DATA_ERROR";
					break;
					case DownloadManager.ERROR_INSUFFICIENT_SPACE:
					failedReason = "ERROR_INSUFFICIENT_SPACE";
					break;
					case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
					failedReason = "ERROR_TOO_MANY_REDIRECTS";
					break;
					case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
					failedReason = "ERROR_UNHANDLED_HTTP_CODE";
					break;
					case DownloadManager.ERROR_UNKNOWN:
					failedReason = "ERROR_UNKNOWN";
					break;
				}

				Toast.makeText(ImageListActivity.this,
					"FAILED: " + failedReason,
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_PAUSED:
				String pausedReason = "";

				switch(reason){
					case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
					pausedReason = "PAUSED_QUEUED_FOR_WIFI";
					break;
					case DownloadManager.PAUSED_UNKNOWN:
					pausedReason = "PAUSED_UNKNOWN";
					break;
					case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
					pausedReason = "PAUSED_WAITING_FOR_NETWORK";
					break;
					case DownloadManager.PAUSED_WAITING_TO_RETRY:
					pausedReason = "PAUSED_WAITING_TO_RETRY";
					break;
				}

				Toast.makeText(ImageListActivity.this,
					"PAUSED: " + pausedReason,
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_PENDING:
				Toast.makeText(ImageListActivity.this,
					"PENDING",
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_RUNNING:
				Toast.makeText(ImageListActivity.this,
					"RUNNING",
					Toast.LENGTH_LONG).show();
				break;
				case DownloadManager.STATUS_SUCCESSFUL:

				Toast.makeText(ImageListActivity.this,
					"SUCCESSFUL",
					Toast.LENGTH_LONG).show();
				//GetFile();
				break;
			}
		}
	}
}