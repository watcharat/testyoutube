package com.example.testyoutube;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

import com.example.testyoutube.RequestHttpClient.RequestHttpClientListenner;

public class MainActivity extends Activity implements RequestHttpClientListenner{

	boolean request_playlist = false;
	boolean request_user = false;
	
	RequestHttpClient httpClient;
	String url_playlist = "http://gdata.youtube.com/feeds/api/playlists/RD021kz6hNDlEEg?v=1&alt=json";
	
	
	//onefotoable  ok
	//awaziavimotihca 
	String url_user = "http://gdata.youtube.com/feeds/api/users/awaziavimotihca/uploads?&v=2&max-results=50&alt=jsonc";  //UCHmpi5o1Fm2PDGa1izasg8w  WishesOnTheEarth
	
	ArrayList<YoutubeObject> array_playlist;
	ArrayList<YoutubeObject> array_user;
	
	double longitude;
	double latitude;
	
	public static String countryCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		
//		LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE); 
//		LocationListener locationListenner = new LocationListener() {
//		    public void onLocationChanged(Location location) {
//		        longitude = location.getLongitude();
//		        latitude = location.getLatitude();
//		        
//		        System.out.println("onLocationChanged");
//		        System.out.println("longitude = "+longitude);
//		        System.out.println("latitude = "+latitude);
//		        
//		        TextView textView1 = (TextView) findViewById(R.id.textView1);
//		        textView1.setText("latitude = "+latitude);
//		        
//		        TextView textView2 = (TextView) findViewById(R.id.textView2);
//		        textView2.setText("longitude = "+longitude);
//		    }
//
//			@Override
//			public void onProviderDisabled(String provider) {
//				// TODO Auto-generated method stub
//				System.out.println("onProviderDisabled provider: "+provider);
//			}
//
//			@Override
//			public void onProviderEnabled(String provider) {
//				// TODO Auto-generated method stub
//				System.out.println("onProviderEnabled provider: "+provider);
//			}
//
//			@Override
//			public void onStatusChanged(String provider, int status,
//					Bundle extras) {
//				// TODO Auto-generated method stub
//				System.out.println("onStatusChanged provider: "+provider+", status: "+status);
//			}
//		};
//
//		lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListenner);
		
		// check if GPS enabled
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation())
        {
            String stringLatitude = String.valueOf(gpsTracker.latitude);
            System.out.println("stringLatitude = "+stringLatitude);

            String stringLongitude = String.valueOf(gpsTracker.longitude);
            System.out.println("stringLongitude = "+stringLongitude);
            
            //doRequestCountry(stringLatitude,stringLongitude);            

//            String country = gpsTracker.getCountryName(this);
//            textview = (TextView)findViewById(R.id.fieldCountry);
//            textview.setText(country);
//
//            String city = gpsTracker.getLocality(this);
//            textview = (TextView)findViewById(R.id.fieldCity);
//            textview.setText(city);
//
//            String postalCode = gpsTracker.getPostalCode(this);
//            textview = (TextView)findViewById(R.id.fieldPostalCode);
//            textview.setText(postalCode);
//
//            String addressLine = gpsTracker.getAddressLine(this);
//            textview = (TextView)findViewById(R.id.fieldAddressLine);
//            textview.setText(addressLine);
        }
        else
        {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            gpsTracker.showSettingsAlert();
        }
        
	}
	
//	private boolean isActivityFinished = false;
//	 
//    @Override
//    public void onPause() {
//    	super.onPause();
//    	if(isActivityFinished) {
//    	    Intent i = new Intent(getApplicationContext(), AdsWrapper.class);
//            i.putExtra("partnerId", "101021");
//            i.putExtra("appId", "com.example.testyoutube");
//            i.putExtra("showAt", "end");
//            i.putExtra("skipEarly", "true");
//            i.putExtra("adsTimeout", "10");
//            startActivity(i);
//    	}
//    }
// 
//    @Override
//    public void finish() {
//        isActivityFinished = true;
//        super.finish();
//    }
	
	private void doRequestCountry(String lat, String lon){
		String url = "http://api.worldweatheronline.com/free/v1/search.ashx?q="+lat+","+lon+"&format=json&key=tc4feefhdfuh75f6pvupfnhz";
		RequestHttpClient locationClient = new RequestHttpClient(url, new RequestHttpClientListenner() {
			@Override
			public void onRequestStringCallback(String response) {
				// TODO Auto-generated method stub
				try {
					JSONObject jsonObject = new JSONObject(response);
					JSONObject search_api = jsonObject.getJSONObject("search_api");
					JSONArray results = search_api.getJSONArray("result");
					if(results.length()>0){
						JSONObject result = results.getJSONObject(0);
						JSONArray country = result.getJSONArray("country");
						countryCode = country.getJSONObject(0).getString("value");
					}
					System.out.println("countryCode = "+countryCode);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, null);
		locationClient.start();
	}

	public void onTestYoutubeClick(View view){
		System.out.println("onTestYoutubeClick");
		
		if(array_user==null){
			if(!request_user)
			request_user = true;
			httpClient = new RequestHttpClient(url_user, this, this);
			httpClient.start();
		}else{
			openListPage(array_user);
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onRequestStringCallback(String response) {
		// TODO Auto-generated method stub
		if(request_user){
			request_user = false;
			try {
				JSONObject res = new JSONObject(response);
				JSONObject data = res.getJSONObject("data");
				JSONArray items = data.getJSONArray("items");
				array_user = new ArrayList<YoutubeObject>();
				for(int i=0; i<items.length(); i++){
					JSONObject item = items.getJSONObject(i);
					YoutubeObject youtubeObject = new YoutubeObject();
					
					youtubeObject.setTitle(item.getString("title"));
					youtubeObject.setCover(item.getJSONObject("thumbnail").getString("sqDefault"));

					JSONObject player = item.getJSONObject("player");
					if(player!=null){
						System.out.println("player = "+player.toString());
						if(player.has("default")){
							String mobile = player.getString("default"); //"http://m.youtube.com/details?v=EVZYQvWsC54"
							mobile = mobile.substring(mobile.indexOf("v=")+2, mobile.indexOf("&feature"));
							youtubeObject.setVcode(mobile);
						}
					}
					
					
					
					array_user.add(youtubeObject);
				}
				openListPage(array_user);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void openListPage(ArrayList<YoutubeObject> array){
		ImageListActivity.array = array;
		Intent intent = new Intent(this, ImageListActivity.class);
		startActivity(intent);
	}
}
